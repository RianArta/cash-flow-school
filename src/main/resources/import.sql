insert into media(media_id, media_nama) values ('M1', 'Offline');
insert into media(media_id, media_nama) values ('M2', 'Online');

insert into kelas(kelas_id, kelas_nama, media_id) values ('K1', 'IPA', 'M1');
insert into kelas(kelas_id, kelas_nama, media_id) values ('K2', 'IPS', 'M2');
insert into kelas(kelas_id, kelas_nama, media_id) values ('K3', 'Bahasa', 'M2');

insert into guru(guru_id, guru_nama, kelas_id) values ('G1', 'Bapak Rusli', 'K1');
insert into guru(guru_id, guru_nama, kelas_id) values ('G2', 'Bapak Gandi', 'K2');
insert into guru(guru_id, guru_nama, kelas_id) values ('G3', 'Ibu Ratih', 'K3');

insert into siswa(siswa_id,siswa_nama, kelas_id) values ('S1', 'alex', 'K1');
insert into siswa(siswa_id,siswa_nama, kelas_id) values ('S2', 'amel', 'K1');
insert into siswa(siswa_id,siswa_nama, kelas_id) values ('S3', 'andi', 'K2');
insert into siswa(siswa_id,siswa_nama, kelas_id) values ('S4', 'bella', 'K2');
insert into siswa(siswa_id,siswa_nama, kelas_id) values ('S5', 'beny', 'K3');
insert into siswa(siswa_id,siswa_nama, kelas_id) values ('S6', 'bintang', 'K3');
