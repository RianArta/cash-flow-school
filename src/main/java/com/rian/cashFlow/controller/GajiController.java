package com.rian.cashFlow.controller;

import com.rian.cashFlow.domain.Gaji;
import com.rian.cashFlow.dto.GajiRequestDTO;
import com.rian.cashFlow.dto.GajiResponseDTO;
import com.rian.cashFlow.service.GajiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/day25_exam/api/v1")
public class GajiController {
    @Autowired
    GajiService gajiService;

    @PostMapping("/postGaji")
    public ResponseEntity<Object>postGaji(
            @RequestBody GajiRequestDTO gajiRequestDTO
    ){
        try{

            Gaji createdGaji = gajiService.createNewGaji(gajiRequestDTO);
            GajiResponseDTO gajiResponseDTO = new GajiResponseDTO(
                    createdGaji.getGuru().getGuruNama(), createdGaji.getGajiTahun(),
                    createdGaji.getGajiBulan(), createdGaji.getGajiNominal());

            return new ResponseEntity<>(gajiResponseDTO, HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>("ERROR", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getGajiByGuruId")
    public ResponseEntity<Object>getGajiByGuruId(
            @RequestParam(value = "guru_id") String guruId
    ){
        try{
            List<Gaji> existingGaji = gajiService.findByGuruId(guruId);

            List<GajiResponseDTO>gajiResponseDTOList = existingGaji.stream().map(
                    gaji -> new GajiResponseDTO(
                            gaji.getGuru().getGuruNama(), gaji.getGajiTahun(),
                            gaji.getGajiBulan(), gaji.getGajiNominal())
            ).collect(Collectors.toList());

            return new ResponseEntity<>(gajiResponseDTOList, HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>("ERROR", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("getGajiByAll")
    public ResponseEntity<Object>getGajiByAll(){
        try{
            List<Gaji>gajiList = gajiService.findAll();

            List<GajiResponseDTO>gajiResponseDTOList = gajiList.stream().map(
                    gaji -> new GajiResponseDTO(
                            gaji.getGuru().getGuruNama(), gaji.getGajiTahun(),
                            gaji.getGajiBulan(), gaji.getGajiNominal())
            ).collect(Collectors.toList());

            return new ResponseEntity<>(gajiResponseDTOList, HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>("ERROR", HttpStatus.BAD_REQUEST);
        }
    }
}
