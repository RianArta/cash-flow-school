package com.rian.cashFlow.controller;

import com.rian.cashFlow.domain.Spp;
import com.rian.cashFlow.dto.SppRequestDTO;
import com.rian.cashFlow.dto.SppResponseDTO;
import com.rian.cashFlow.service.SppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/day25_exam/api/v1")
public class SppController {
    @Autowired
    SppService sppService;

    @PostMapping("postSpp")
    public ResponseEntity<Object>postSpp(
            @RequestBody SppRequestDTO sppRequestDTO
    ){
        try{
            Spp newSpp = sppService.createNewSpp(sppRequestDTO);
            SppResponseDTO sppResponseDTO = new SppResponseDTO(
                    newSpp.getSiswa().getSiswaNama(), newSpp.getSppTahun(),
                    newSpp.getSppBulan(), newSpp.getSppNominal());

            return new ResponseEntity<>(sppResponseDTO, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("ERROR", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getSppByTahunBulan")
    public ResponseEntity<Object>getSppByTahunBulan(
            @RequestParam(value = "tahun") String tahunParam,
            @RequestParam(value = "bulan") String bulanParam
    ){
        try{
            List<Spp> existingSppByTahunBulan = sppService.findSppByTahunBulan(tahunParam, bulanParam);

            List<SppResponseDTO>sppResponseDTOList = existingSppByTahunBulan.stream().map(
                    spp -> new SppResponseDTO(
                            spp.getSiswa().getSiswaNama(), spp.getSppTahun(),
                            spp.getSppBulan(), spp.getSppNominal())
            ).collect(Collectors.toList());

            return new ResponseEntity<>(sppResponseDTOList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("ERROR", HttpStatus.BAD_REQUEST);
        }
    }
}
