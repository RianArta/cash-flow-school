package com.rian.cashFlow.controller;

import com.rian.cashFlow.domain.CashFlow;
import com.rian.cashFlow.dto.CashFlowResponseDTO;
import com.rian.cashFlow.service.CashFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/day25_exam/api/v1")
public class CashFlowController {
    @Autowired
    CashFlowService cashFlowService;

    @GetMapping("/getCashflowByAll")
    public ResponseEntity<Object>getCashflowByAll(){
//        try {
            List<CashFlow>cashFlowList = cashFlowService.findAll();
            List<CashFlowResponseDTO>cashFlowResponseDTOList = new ArrayList<>();

            for(CashFlow cashFlow : cashFlowList){
                CashFlowResponseDTO cashFlowResponseDTO = new CashFlowResponseDTO();
                cashFlowResponseDTO.setCash_id(cashFlow.getCashId());

                if(cashFlow.getGaji()==null){
                    cashFlowResponseDTO.setGaji_id("");
                    cashFlowResponseDTO.setSpp_id(cashFlow.getSpp().getSppId());
                }else if(cashFlow.getSpp()==null){
                    cashFlowResponseDTO.setGaji_id(cashFlow.getGaji().getGajiId());
                    cashFlowResponseDTO.setSpp_id("");
                }

                cashFlowResponseDTO.setCash_debet(cashFlow.getCashDebet());
                cashFlowResponseDTO.setCash_kredit(cashFlow.getCashKredit());
                cashFlowResponseDTO.setCash_tipe(cashFlow.getCashTipe());

                cashFlowResponseDTOList.add(cashFlowResponseDTO);
            }
            return new ResponseEntity<>(cashFlowResponseDTOList, HttpStatus.OK);
//        }
//        catch (Exception e){
//            return new ResponseEntity<>("ERROR", HttpStatus.BAD_REQUEST);
//        }
    }
}
