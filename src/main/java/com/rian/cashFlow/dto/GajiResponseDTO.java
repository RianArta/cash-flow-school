package com.rian.cashFlow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GajiResponseDTO {
    private String guru;
    private String tahun;
    private String bulan;
    private int gaji;
}
