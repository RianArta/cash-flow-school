package com.rian.cashFlow.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SppRequestDTO {
    private String sppId;
    private String siswaId;
    private String sppTahun;
    private String sppBulan;
}
