package com.rian.cashFlow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SppResponseDTO {
    private String siswa;
    private String tahun;
    private String bulan;
    private int spp;
}
