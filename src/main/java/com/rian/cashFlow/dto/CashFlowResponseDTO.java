package com.rian.cashFlow.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CashFlowResponseDTO {
    private int cash_id;
    private String gaji_id;
    private String spp_id;
    private int cash_debet;
    private int cash_kredit;
    private String cash_tipe;
}
