package com.rian.cashFlow.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GajiRequestDTO {
    private String gajiId;
    private String guruId;
    private String gajiTahun;
    private String gajiBulan;
}
