package com.rian.cashFlow.service;

import com.rian.cashFlow.domain.CashFlow;
import com.rian.cashFlow.domain.Gaji;
import com.rian.cashFlow.domain.Guru;
import com.rian.cashFlow.dto.GajiRequestDTO;
import com.rian.cashFlow.repo.CashFlowRepository;
import com.rian.cashFlow.repo.GajiRepository;
import com.rian.cashFlow.repo.GuruRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GajiService {
    @Autowired
    GajiRepository gajiRepository;
    @Autowired
    GuruRepository guruRepository;
    @Autowired
    CashFlowRepository cashFlowRepository;

    public Gaji createNewGaji(GajiRequestDTO gajiRequestDTO){
        Gaji newGaji = new Gaji();

        newGaji.setGajiId(gajiRequestDTO.getGajiId());

        Guru guruId = guruRepository.getById(gajiRequestDTO.getGuruId());

        newGaji.setGuru(guruId);
        newGaji.setGajiBulan(gajiRequestDTO.getGajiBulan());
        newGaji.setGajiTahun(gajiRequestDTO.getGajiTahun());

        String mediaName = guruRepository.findById(gajiRequestDTO.getGuruId())
                                        .get().getKelas().getMedia().getMediaNama();
        int nominal = 0;
        if(mediaName.equals("Offline")){
            nominal = 5000;
        }else if (mediaName.equals("Online")){
            nominal = 3000;}

        newGaji.setGajiNominal(nominal);
        Gaji savedGaji = gajiRepository.save(newGaji);

        CashFlow newCashFlow = new CashFlow();
        Optional<Gaji>getGaji = gajiRepository.findById(gajiRequestDTO.getGajiId());
        newCashFlow.setGaji(getGaji.get());
        newCashFlow.setSpp(null);
        newCashFlow.setCashDebet(nominal);
        newCashFlow.setCashTipe("gaji");

        CashFlow savedCashFlow = cashFlowRepository.save(newCashFlow);

        return savedGaji;
    }

    public List<Gaji> findByGuruId(String guruId) {
        return gajiRepository.findAllByGuruId(guruId);
    }

    public List<Gaji>findAll(){
        return gajiRepository.findAll();
    }
}
