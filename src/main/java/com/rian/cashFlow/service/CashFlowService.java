package com.rian.cashFlow.service;

import com.rian.cashFlow.domain.CashFlow;
import com.rian.cashFlow.repo.CashFlowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CashFlowService {
    @Autowired
    CashFlowRepository cashFlowRepository;

    public List<CashFlow>findAll(){
        return cashFlowRepository.findAll();
    }
}
