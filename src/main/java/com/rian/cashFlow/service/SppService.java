package com.rian.cashFlow.service;

import com.rian.cashFlow.domain.CashFlow;
import com.rian.cashFlow.domain.Siswa;
import com.rian.cashFlow.domain.Spp;
import com.rian.cashFlow.dto.SppRequestDTO;
import com.rian.cashFlow.repo.CashFlowRepository;
import com.rian.cashFlow.repo.SiswaRepository;
import com.rian.cashFlow.repo.SppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SppService {
    @Autowired
    SppRepository sppRepository;
    @Autowired
    SiswaRepository siswaRepository;
    @Autowired
    CashFlowRepository cashFlowRepository;

    public Spp createNewSpp(SppRequestDTO sppRequestDTO){
        Spp newSpp = new Spp();

        newSpp.setSppId(sppRequestDTO.getSppId());

        Siswa getSiswaId = siswaRepository.getById(sppRequestDTO.getSiswaId());
        newSpp.setSiswa(getSiswaId);
        newSpp.setSppTahun(sppRequestDTO.getSppTahun());
        newSpp.setSppBulan(sppRequestDTO.getSppBulan());

        String mediaName = siswaRepository.findById(sppRequestDTO.getSiswaId())
                            .get().getKelas().getMedia().getMediaNama();

        int nominal = 0;
        if(mediaName.equals("Offline")){
            nominal = 5000;
        }else if (mediaName.equals("Online")){
            nominal = 3000;}

        newSpp.setSppNominal(nominal);
        Spp savedSpp = sppRepository.save(newSpp);

        CashFlow newCashFlow = new CashFlow();
        Optional<Spp>getSpp = sppRepository.findById(sppRequestDTO.getSppId());
        newCashFlow.setSpp(getSpp.get());
        newCashFlow.setGaji(null);
        newCashFlow.setCashKredit(nominal);
        newCashFlow.setCashTipe("spp");

        CashFlow savedCashFlow = cashFlowRepository.save(newCashFlow);

        return savedSpp;
    }

    public List<Spp>findSppByTahunBulan(String tahun, String bulan){
        return sppRepository.findByTahunBulan(tahun, bulan);
    }
}
