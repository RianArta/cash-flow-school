package com.rian.cashFlow.repo;

import com.rian.cashFlow.domain.CashFlow;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CashFlowRepository extends JpaRepository<CashFlow, Integer> {
}
