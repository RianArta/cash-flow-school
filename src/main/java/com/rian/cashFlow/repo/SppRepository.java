package com.rian.cashFlow.repo;

import com.rian.cashFlow.domain.Spp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface SppRepository extends JpaRepository<Spp, String > {

    @Query(value = "select * from spp where spp_tahun = ?1 AND spp_bulan = ?2", nativeQuery = true)
    List<Spp> findByTahunBulan(String tahun, String bulan);

}
