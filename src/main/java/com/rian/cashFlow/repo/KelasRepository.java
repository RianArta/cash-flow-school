package com.rian.cashFlow.repo;

import com.rian.cashFlow.domain.Kelas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KelasRepository extends JpaRepository<Kelas, String > {
}
