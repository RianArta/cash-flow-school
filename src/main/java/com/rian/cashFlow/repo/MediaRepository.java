package com.rian.cashFlow.repo;

import com.rian.cashFlow.domain.Media;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaRepository extends JpaRepository<Media, String > {
}
