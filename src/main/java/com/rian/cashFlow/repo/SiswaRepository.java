package com.rian.cashFlow.repo;

import com.rian.cashFlow.domain.Siswa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SiswaRepository extends JpaRepository<Siswa, String > {
}
