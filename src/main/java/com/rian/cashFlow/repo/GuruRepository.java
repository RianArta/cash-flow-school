package com.rian.cashFlow.repo;

import com.rian.cashFlow.domain.Guru;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuruRepository extends JpaRepository<Guru, String > {
}
