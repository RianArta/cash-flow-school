package com.rian.cashFlow.repo;

import com.rian.cashFlow.domain.Gaji;
import com.rian.cashFlow.domain.Spp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GajiRepository extends JpaRepository<Gaji, String > {
    @Query(value = "select * from gaji where guru_id = ?1", nativeQuery = true)
    List<Gaji> findAllByGuruId(String guruId);

}
