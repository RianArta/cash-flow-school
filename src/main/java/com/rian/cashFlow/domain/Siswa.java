package com.rian.cashFlow.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "siswa")
public class Siswa {
    @Id
    @Column(name = "siswa_id")
    private String siswaId;

    @Column(name = "siswa_nama")
    private String siswaNama;

    @ManyToOne
    @JoinColumn(name = "kelas_id")
    private Kelas kelas;
}
