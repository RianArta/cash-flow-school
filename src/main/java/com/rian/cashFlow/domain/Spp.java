package com.rian.cashFlow.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "spp")
@Builder
public class Spp {
    @Id
    @Column(name = "spp_id")
    private String sppId;

    @ManyToOne
    @JoinColumn(name = "siswa_id")
    private Siswa siswa;

    @Column(name = "spp_tahun")
    private String sppTahun;

    @Column(name = "spp_bulan")
    private String sppBulan;

    @Column(name = "spp_nominal")
    private int sppNominal;
}
