package com.rian.cashFlow.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "kelas")
public class Kelas {
    @Id
    @Column(name = "kelas_id")
    private String kelasId;

    @Column(name = "kelas_nama")
    private String kelasNama;

    @ManyToOne
    @JoinColumn(name = "media_id")
    private Media media;
}
