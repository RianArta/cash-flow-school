package com.rian.cashFlow.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "media")
public class Media {
    @Id
    @Column(name = "media_id")
    private String mediaId;

    @Column(name = "media_nama")
    private String mediaNama;

}
