package com.rian.cashFlow.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "gaji")
@Builder
public class Gaji {
    @Id
    @Column(name = "gaji_id")
    private String gajiId;

    @ManyToOne
    @JoinColumn(name = "guru_id")
    private Guru guru;

    @Column(name = "gaji_tahun")
    private String gajiTahun;

    @Column(name = "gaji_bulan")
    private String gajiBulan;

    @Column(name = "gaji_nominal")
    private int gajiNominal;


}
