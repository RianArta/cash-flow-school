package com.rian.cashFlow.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cashflow")
public class CashFlow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cash_id")
    private int cashId;

    @ManyToOne
    @JoinColumn(name = "gaji_id")
    private Gaji gaji;

    @ManyToOne
    @JoinColumn(name = "spp_id")
    private Spp spp;

    @Column(name = "cash_debet")
    private int cashDebet;

    @Column(name = "cash_kredit")
    private int cashKredit;

    @Column(name = "cash_tipe")
    private String  cashTipe;

}
