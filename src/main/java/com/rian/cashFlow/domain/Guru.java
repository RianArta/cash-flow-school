package com.rian.cashFlow.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "guru")
public class Guru {
    @Id
    @Column(name = "guru_id")
    private String guruId;

    @Column(name = "guru_nama")
    private String guruNama;

    @OneToOne
    @JoinColumn(name = "kelas_id")
    private Kelas kelas;

}
